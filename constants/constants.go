package constants

const (
	// V1 prefix
	V1 string = "v1"

	// HealthPath const
	HealthPath string = "health"

	// Message key
	Message string = "message"

	// OptionsVerb for HTTP OPTIONS
	OptionsVerb string = "OPTIONS"

	// GetHTTPVerb method
	GetHTTPVerb string = "GET"

	// PostHTTPVerb method
	PostHTTPVerb string = "POST"

	// PutHTTPVerb method
	PutHTTPVerb string = "PUT"

	// PutHTTPVerb method
	PatchHTTPVerb string = "PATCH"

	// DeleteHTTPVerb method
	DeleteHTTPVerb string = "DELETE"

	// AuthDomainKey for lookup
	AuthDomainKey string = "auth.domain"

	// AuthJWKSKey for lookup
	AuthJWKSKey string = "auth.jwks"

	// AuthAudienceKey for lookup
	AuthAudienceKey string = "auth.audience"

	// InvalidPayloadMsg const
	InvalidPayloadMsg string = "Payload is invalid"

	// UnableToProcessRequestMsg const
	UnableToProcessRequestMsg string = "There was an issue processing the request."
)
