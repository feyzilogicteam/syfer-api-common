NAME=syfer-api-common
VERSION=0.1.0

export GO111MODULE=on

.PHONY: build
build: get
	@go build -o $(NAME)

.PHONY: run
run: build
	@./$(NAME) -stderrthreshold=INFO -logtostderr=true

.PHONY: clean
clean:
	@rm -f $(NAME)

.PHONY: get
get:
	@go mod download