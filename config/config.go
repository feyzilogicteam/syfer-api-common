package config

import (
	"log"
	"os"
	"strconv"

	"github.com/golang/glog"

	"github.com/spf13/viper"
)

var config *viper.Viper

// Init is an exported method that takes the environment starts the viper
// (external lib) and returns the configuration struct.
func Init(name string) {
	var err error
	v := viper.New()
	v.SetConfigType("yaml")
	v.SetConfigName(name)
	v.AddConfigPath("config/")
	v.AddConfigPath("../config/")
	err = v.ReadInConfig()
	if err != nil {
		log.Fatalf("error on parsing configuration file: %v", name)
	}
	config = v
}

// GetConfig returns the config file object.
func GetConfig() *viper.Viper {
	return config
}

// GetConfigString returns the value associated with the key as a string.
func GetConfigString(key string) string {
	return config.GetString(key)
}

// GetConfigBool returns the value associated with the key as a boolean.
func GetConfigBool(key string) bool {
	val, _ := strconv.ParseBool(config.GetString(key))
	return val
}

// GetEnvString returns the value associated with the env variable as a string
func GetEnvString(key string) string {
	return os.Getenv(key)
}

// GetEnvBool returns the value associated with the env variable as a boolean
func GetEnvBool(key string) bool {
	val, _ := strconv.ParseBool(os.Getenv(key))
	return val
}

// ParseUInt64 will parse a string and return uint64
func ParseUInt64(u string) uint64 {
	id, err := strconv.ParseUint(u, 0, 64)
	if err != nil {
		glog.Fatalf("Unable to parse '%s'", u)
	}

	return id
}

// ParseUInt8 will parse a string and return uint8
func ParseUInt8(s string) uint8 {
	val, _ := strconv.ParseUint(s, 10, 8)
	return uint8(val)
}
