module bitbucket.org/feyzilogicteam/syfer-api-common

go 1.12

require (
	github.com/auth0-community/go-auth0 v1.0.0
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/gin v1.5.0
	github.com/golang/glog v0.0.0-20160126235308-23def4e6c14b
	github.com/spf13/viper v1.4.0
	gopkg.in/square/go-jose.v2 v2.3.1
)
