package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// Status used to check the health of the API
func Health(c *gin.Context) {
	c.String(http.StatusOK, "Working!")
}
