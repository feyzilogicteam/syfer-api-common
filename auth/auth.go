package auth

import (
	"net/http"
	"time"

	auth0 "github.com/auth0-community/go-auth0"
	"github.com/gin-gonic/gin"
	"github.com/golang/glog"
	"gopkg.in/square/go-jose.v2"

	"bitbucket.org/feyzilogicteam/syfer-api-common/config"
	"bitbucket.org/feyzilogicteam/syfer-api-common/constants"
)

func GetConfiguration(domain, jwks, aud string) (auth0.SecretProvider, auth0.Configuration) {
	return GetConfigurationWithDuration(domain, jwks, aud, time.Hour*time.Duration(24))
}

func GetConfigurationWithDuration(domain, jwks, aud string, duration time.Duration) (auth0.SecretProvider, auth0.Configuration) {
	keyCacher := auth0.NewMemoryKeyCacher(duration, 50)
	clientOptions := auth0.JWKClientOptions{URI: domain + jwks}
	provider := auth0.NewJWKClientWithCache(clientOptions, nil, keyCacher)
	configuration := auth0.NewConfiguration(provider, []string{aud}, domain, jose.RS256)

	return provider, configuration
}

// Middleware to handle JWT validation with Auth0
func Middleware() gin.HandlerFunc {
	configuration := getConfiguration()
	validator := auth0.NewValidator(configuration, nil)

	return func(c *gin.Context) {
		_, err := validator.ValidateRequest(c.Request)
		if err != nil {
			glog.Warning(err.Error())
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		if c.Request.Method == constants.OptionsVerb {
			c.AbortWithStatus(http.StatusNoContent)
			return
		}

		c.Next()
	}
}

func getConfiguration() auth0.Configuration {
	cfg := config.GetConfig()
	domain := cfg.GetString(constants.AuthDomainKey)
	jwks := cfg.GetString(constants.AuthJWKSKey)
	aud := cfg.GetString(constants.AuthAudienceKey)
	_, configuration := GetConfiguration(domain, jwks, aud)

	return configuration
}
