package util

import (
	"fmt"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"

	"bitbucket.org/feyzilogicteam/syfer-api-common/constants"
	"bitbucket.org/feyzilogicteam/syfer-api-common/controller"
)

// NewRouter that includes all routes
func NewRouter() *gin.Engine {
	router := gin.Default()
	gin.SetMode(gin.ReleaseMode)
	router.Use(
		gin.LoggerWithWriter(gin.DefaultWriter, fmt.Sprintf("/%s", constants.HealthPath)),
		handlerFunc())
	router.GET(constants.HealthPath, controller.Health)
	return router
}

func handlerFunc() gin.HandlerFunc {
	return func(c *gin.Context) {
		cors.Default()
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Max-Age", "86400")
		c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH")
		c.Writer.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Content-Length, Accept-Encoding, Authorization")
		c.Writer.Header().Set("Access-Control-Expose-Headers", "Content-Length")
		c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")

		if c.Request.Method == constants.OptionsVerb {
			c.AbortWithStatus(204)
		} else {
			c.Next()
		}
	}
}
